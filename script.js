let group = document.querySelector(".group__input");
let fname = document.querySelector(".fname__input");
let lname = document.querySelector(".lname__input");
let gender = document.querySelector(".gender__input");
let birthday = document.querySelector(".birthday__input");

document.querySelector(".button-class").addEventListener("click", () => {
    group.value = "";
    fname.value = "";
    lname.value = "";
    gender.value = "";
    birthday.value = "";
    group.style.borderColor = "";
    fname.style.borderColor = "";
    lname.style.borderColor = "";
    gender.style.borderColor = "";
    birthday.style.borderColor = "";
    group.style.borderWidth = "1px";
    fname.style.borderWidth = "1px";
    lname.style.borderWidth = "1px";
    gender.style.borderWidth = "1px";
    birthday.style.borderWidth = "1px";
    document.querySelector(".adding__page").style.display="flex";
    document.querySelector(".confirm__adding").addEventListener("click", f);
    document.querySelector(".confirm__adding").style.display = "block"
    document.querySelector(".confirm__edit").style.display = "none"
});

function validateFields() {
    const values = [group, fname, lname, gender, birthday];
    let allFilled = true;

    for (let i = 0; i < values.length; i++) {
        if (values[i].value === "") {
            allFilled = false;
            values[i].style.borderColor = "red";
            values[i].style.borderWidth = "3px";
        } else {
            values[i].style.borderColor = "";
            values[i].style.borderWidth = "1px";
        }
    }
    return allFilled;
}


function f() {
    const parent = document.querySelector(".table__body");
    const child = document.createElement("tr");
    child.className = "Row";
    child.innerHTML = `<td class="table__content table__checkbox">
        <input type="checkbox" name="checkbox${document.getElementsByTagName('tr').length + 1}" class="checkbox">
        </td>
        
        <td class="table__content table__group">${group.value}</td>
        <td class="table__content table__name">${fname.value + " " + lname.value}</td>
        <td class="table__content table__gender">${gender.value}</td>
        <td class="table__content table__birthday">${birthday.value}</td>
        <td class="table__content table__status">
            <div class="circle">
            </div>
        </td>
        <td class="table__content options">
            <button class="options__edit">
                <img src="edit.png">
            </button>
            <button class="options__delete">
                <img src="delete.png">
            </button>
        </td>`;

    if (validateFields()) {
        const data = {
            group:group.value,
            fname: fname.value,
            lname: lname.value,
            gender:gender.value,
            birthday:birthday.value

        }
        console.log(data)
        fetch('/check-user', {

            method: 'POST',
            body: JSON.stringify(data),

            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
            .then((response) => response.json())
            .then((json) => console.log(json));



        parent.appendChild(child);
        document.querySelector(".adding__page").style.display = "none";

        // Обробники подій для кнопок редагування та видалення
        child.querySelector(".options__edit").addEventListener("click", () => {
            document.querySelector(".adding__page").style.display = "flex";
            document.querySelector(".chaning__title").innerHTML = "Edit";
            document.querySelector(".confirm__adding").style.display = "none";
            document.querySelector(".confirm__edit").style.display = "block";

            // Обробник події для підтвердження редагування
            document.querySelector(".confirm__edit").addEventListener("click", () => {
                // Перевірка валідності перед закриттям вікна редагування
                if (validateFields()) {
                    const row_changing = child;
                    const children = row_changing.querySelectorAll(".table__content");

                    let group = document.querySelector(".group__input");
                    let fname = document.querySelector(".fname__input");
                    let lname = document.querySelector(".lname__input");
                    let gender = document.querySelector(".gender__input");
                    let birthday = document.querySelector(".birthday__input");

                    // Проведення редагування запису, оскільки всі поля заповнені
                    children[1].textContent = group.value;
                    children[2].textContent = fname.value + " " + lname.value;
                    children[3].textContent = gender.value;
                    children[4].textContent = birthday.value;

                    // Закриття вікна редагування після успішного редагування
                    document.querySelector(".adding__page").style.display = "none";
                } else {
                    // Повідомлення про помилку, якщо хоча б одне поле порожнє
                    document.querySelector(".adding__page").style.display = "flex";
                }
            });
        });


        child.querySelector(".options__delete").addEventListener("click", () => {
            document.querySelector(".warning__page").style.display = "flex";
            document.querySelector(".confirm__warning").addEventListener("click", () => {
                child.remove();
                document.querySelector(".warning__page").style.display = "none";
            });
        });
    } else {
        // Якщо хоча б одне поле порожнє, не додаємо запис і можливо відображаємо повідомлення про помилку
        // Тут ви можете додати код для відображення повідомлення про помилку
    }
}

document.querySelector(".confirm__adding").addEventListener("click", f);

// Checkboxes
const first = document.querySelector(".checkbox");
first.addEventListener("change", () => {
    document.querySelectorAll(".checkbox").forEach(item => {
        item.checked = first.checked;
    });
});
document.getElementById('task').addEventListener('click', function() {
    document.getElementById('students-section').style.display = 'none';
    document.getElementById('tasks-section').style.display = 'block';
});
document.getElementById('student').addEventListener('click', function() {
    document.getElementById('students-section').style.display = 'block';
    document.getElementById('tasks-section').style.display = 'none';
});

